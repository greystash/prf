import React from "react";
import PropTypes from "prop-types";
import { Card, Elevation } from "@blueprintjs/core";
import styles from "./BikeCard.module.scss";
import classNames from "classnames/bind";

let cn = classNames.bind(styles);

class BikeCard extends React.Component {
  constructor(props) {
    super(props);

    this.getImg = this.getImg.bind(this);
  }

  getImg(bike) {
    if (bike.bds_card !== null) {
      if (bike.bds_card.img_1 !== "") {
        return bike.bds_card.img_1;
      } else {
        return bike.bds_card.img_1;
      }
    } else if (bike.aucnet_card !== null) {
      if (bike.aucnet_card !== null && bike.aucnet_card.img_1 !== "") {
        return bike.aucnet_card.img_1;
      }
    } else if (bike.jba_card !== null && bike.jba_card.img_1 !== "") {
      return bike.jba_card.img_1;
    } else if (bike.images !== null && bike.images !== "") {
      return bike.images.split(",")[0];
    } else {
      return "https://image.freepik.com/free-vector/motorcycle-vector-logo_20448-45.jpg";
    }
  }

  render() {
    const { fake, bike } = this.props;

    if (fake === true) {
      return (
        <Card
          className={cn("bp3-dark", styles.card)}
          elevation={Elevation.ONE}
          interactive={true}
        >
          <img className={cn("bp3-skeleton")} />
          <div className={cn(styles.infoWrapper)}>
            <div className={cn('bp3-skeleton', styles.title)}>
              <h2 className={cn()}>xxx</h2>
            </div>
            <div className={cn('bp3-skeleton', styles.mainInfo)}>
              <div className={cn(styles.auction)}>
                <p>xxx</p>
                <p>xxx</p>
                <p>xxx</p>
                <p>xxx</p>
              </div>
              <div className={cn(styles.bike)}>
                <p>xxx</p>
                <p>xxx</p>
                <p>xxx</p>
                <p>xxx</p>
              </div>
            </div>
          </div>
        </Card>
      );
    } else {
      return (
        <Card
          className={cn("bp3-dark", styles.card)}
          elevation={Elevation.ONE}
          interactive={true}
        >
          <img className={cn("bp3-skeleton")} src={this.getImg(bike)} />
          <div className={cn(styles.infoWrapper)}>
            <div className={cn(styles.title)}>
              <h2 className={cn()}>
                {bike.manufacturer.manufacturer === null
                  ? bike.manufacturer.name
                  : bike.manufacturer.manufacturer.name}{" "}
                -{" "}
                {bike.bike_model.bike_model === null
                  ? bike.bike_model.name
                  : bike.bike_model.bike_model.name}
              </h2>
              <h4 className={cn()}>{bike.calculated_price}</h4>
            </div>
            <div className={cn(styles.mainInfo)}>
              <div className={cn(styles.auction)}>
                <p>
                  {bike.auction.auction} - {bike.auction.date}
                </p>
                <p>Номер лота - {bike.lot_number} </p>
                <p>Место аукциона - {bike.location}</p>
              </div>
              <div className={cn(styles.bike)}>
                <p>Год - {bike.year}</p>
                <p>Объем двигателя - {bike.displacement}</p>
                <p>Номер рамы - {bike.frame_serial}</p>
                <p>Пробег - {bike.mileage}КМ</p>
              </div>
            </div>
          </div>
        </Card>
      );
    }
  }
}

BikeCard.protoTypes = {
  fake: PropTypes.bool.isRequired,
  bike: PropTypes.object
};

export default BikeCard;
