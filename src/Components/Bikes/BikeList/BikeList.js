import React from "react";
import { HTMLTable, Card, Elevation } from "@blueprintjs/core";
import BikesApi from "./../../../Api/BikesApi.js";
import styles from "./BikeList.module.scss";
import classNames from "classnames/bind";
import InfiniteScroll from "react-infinite-scroll-component";
import BikeCard from "./../BikeCard/BikeCard.js";

const bikesApi = new BikesApi();
let cn = classNames.bind(styles);

class BikeList extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      bikes: [],
      fakeBikes: [],
      totalBikes: 0,
      isLoading: false,
      currentBikes: 0,
      nextUrl: '',
      prevUrl: '',
    };

    this.getBikes = this.getBikes.bind(this);
  }

  componentDidMount() {
    this.getBikes();
  }

  getBikes() {
    let fakes = [];
    for (let i = 0; i < 30; i++) {
      fakes.push("fake");
    }

    this.setState(prevState => ({
      isLoading: true,
      fakeBikes: [...prevState.bikes, ...fakes]
    }));

    bikesApi.getBikes().then(res => {
      this.setState(prevState => ({
        ...prevState,
        bikes: [...prevState.bikes, ...res.results],
        totalBikes: res.count,
        nextUrl: res.next,
        prevUrl: res.previous,
        currentBikes: prevState.currentBikes + res.results.length,
        isLoading: false
      }));
    });
  }

  render() {
    if (this.state.isLoading === true) {
      return (
        <div className={styles.bikeList}>
          {this.state.fakeBikes.map(b =>
            b === "fake" ? (
              <BikeCard fake={true}/>
            ) : (
              <BikeCard fake={false} bike={b} />
            )
          )}
        </div>
      );
    } else if (this.state.currentBikes > 0) {
      return (
        <div className={styles.bikeList}>
        <button onClick={this.getBikes} >hahaha</button>
          {this.state.bikes.map(b => (
            <BikeCard fake={false} bike={b} />
          ))}
        </div>
      );
    } else {
      return <div onClick={this.getBikes}> No content </div>;
    }
  }
}

export default BikeList;
