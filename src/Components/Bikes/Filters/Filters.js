import React from "react";
import {
  RangeSlider,
  Collapse,
  Card,
  NumericInput,
  Label,
  Tab,
  Tabs,
  Button,
  ButtonGroup,
  FormGroup,
  Divider
} from "@blueprintjs/core";
import classNames from "classnames/bind";
import styles from "./Filters.module.scss";

let cn = classNames.bind(styles);

class Filters extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      disp_min: 100,
      disp_max: 100,
      year_min: 100,
      year_max: 100,
      date_min: 0,
      date_max: 0,
      mile_min: 0,
      mile_max: 0,
      manufacturer: [],
      models: [],
      auctions: [],
      frame: "",
      lot: 0,
      rank_min: 0,
      rank_max: 0,
      displacement_list: [],

      filtersOpen: true,
      orderingOpen: false
    };

    this.handleChange = this.handleChange.bind(this);
    this.changeFiltering = this.changeFiltering.bind(this);
    this.changeOrdering = this.changeOrdering.bind(this);
  }

  componentDidMount() {}

  handleChange(val, val_str, el) {
    const name = el.name;

    this.setState({
      ...this.state,
      [name]: val_str
    });
  }

  changeFiltering() {
    this.setState(prevState => ({
      ...prevState,
      filtersOpen: !prevState.filtersOpen,
      orderingOpen: false
    }));
  }

  changeOrdering() {
    this.setState(prevState => ({
      ...prevState,
      filtersOpen: false,
      orderingOpen: !prevState.orderingOpen
    }));
  }

  render() {
    return (
      <div className={cn(styles.filters)}>
        <Card className={cn("bp3-dark")}>
          <ButtonGroup>
            <Button
              text="Filters"
              icon="database"
              onClick={this.changeFiltering}
              className={cn({ "bp3-active": this.state.filtersOpen })}
            />
            <Button
              text="Ordering"
              icon="sort"
              onClick={this.changeOrdering}
              className={cn({ "bp3-active": this.state.orderingOpen })}
            />
            <Button
              text="Update"
              icon="refresh"
              className={cn("bp3-intent-primary")}
            />
          </ButtonGroup>

          <Collapse isOpen={this.state.filtersOpen}>
            <div className={cn(styles.aucs)}></div>
            <div className={cn(styles.numsWrapper)}>
              <div className={styles.numWrapper}>
                <h6>Кубы</h6>
                <Divider verical={true} />
                <Label htmlFor="disp_min">От</Label>
                <NumericInput
                  min={100}
                  max={2000}
                  stepSize={25}
                  majorStepSize={25}
                  id="disp_min"
                  name="disp_min"
                  placeholder="cm3"
                  onValueChange={this.handleChange}
                />
                <Label htmlFor="disp_max">До</Label>
                <NumericInput
                  min={100}
                  max={2000}
                  stepSize={25}
                  majorStepSize={25}
                  id="disp_max"
                  name="disp_max"
                  placeholder="cm3"
                  onValueChange={this.handleChange}
                />
              </div>
              <div className={styles.numWrapper}>
                <h6>Год</h6>
                <Divider vertical={true} />
                <Label htmlFor="year_min">От</Label>
                <NumericInput
                  min={1900}
                  className={cn(styles.numInp)}
                  stepSize={1}
                  majorStepSize={25}
                  id="year_min"
                  name="year_min"
                  placeholder="гггг"
                  onValueChange={this.handleChange}
                />
                <Label htmlFor="year_max">До</Label>
                <NumericInput
                  min={this.state.year_min}
                  stepSize={1}
                  majorStepSize={25}
                  id="year_max"
                  name="year_max"
                  placeholder="гггг"
                  onValueChange={this.handleChange}
                />
              </div>
              <div className={styles.numWrapper}>
                <h6>Пробег</h6>
                <Divider verical={true} />
                <Label htmlFor="mile_min">От</Label>
                <NumericInput
                  min={0}
                  stepSize={100}
                  majorStepSize={100}
                  id="mile_min"
                  name="mile_min"
                  placeholder="km"
                  onValueChange={this.handleChange}
                />
                <Label htmlFor="mile_max">До</Label>
                <NumericInput
                  min={this.state.mile_min}
                  stepSize={100}
                  majorStepSize={100}
                  id="mile_max"
                  name="mile_max"
                  placeholder="km"
                  onValueChange={this.handleChange}
                />
              </div>
              <div className={styles.numWrapper}>
                <h6>Оценка</h6>
                <Divider verical={true} />
                <Label htmlFor="rank_min">От</Label>
                <NumericInput
                  min={0}
                  max={5}
                  stepSize={1}
                  majorStepSize={1}
                  id="rank_min"
                  name="rank_min"
                  onValueChange={this.handleChange}
                />
                <Label htmlFor="rank_max">До</Label>
                <NumericInput
                  min={this.state.rank_min}
                  stepSize={1}
                  majorStepSize={1}
                  max={5}
                  id="rank_max"
                  name="rank_max"
                  onValueChange={this.handleChange}
                />
              </div>
            </div>
          </Collapse>

          <Collapse isOpen={this.state.orderingOpen}></Collapse>
        </Card>
      </div>
    );
  }
}

export default Filters;
