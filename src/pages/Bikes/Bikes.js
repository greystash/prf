import React, { useState, useEffect } from "react";
import BikeList from "./../../Components/Bikes/BikeList/BikeList.js";
import Filters from "./../../Components/Bikes/Filters/Filters.js";
import { bikeList } from "./Bikes.module.scss";

function Bikes() {
  return (
    <div className="">
      <Filters />
      <BikeList className="" />
    </div>
  );
}

export default Bikes;
