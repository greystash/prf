import axios from 'axios';
const API_URL = 'https://projtest2.ru';

export default class BikesApi {
  constructor () {}

  getBikes(params) {
    const url = `${API_URL}/bikes/`;
    return axios.get(url, { params: params }).then(response => response.data);
  }

  getBikesByUrl(link) {
    const url = `${link}`;
    return axios.get(url).then(response => response.data);
  }

  getBike(pk) {
    const url = `${API_URL}/bikes/${pk}/`;
    return axios.get(url).then(response => response.data);
  }

  getBikePrice(bm) {
    const url = `${API_URL}/price/`;
    return axios.get(url, { params: bm }).then(response => response.data)
  }

  getModelss(params) {
    const url = `${API_URL}/models/`;
    return axios.get(url, { params: params }).then(response => response.data);
  }

  getManuf(params) {
    const url = `${API_URL}/manuf`;
    return axios.get(url, { params: params }).then(response => response.data);
  }

  getModelsByUrl(link, params) {
    const url = `${link}`;
    return axios.get(url, {params: params}).then(response => response.data);
  }

  updateModels(params) {
    const url = `${API_URL}/up_md`;
    return axios.post(url, params).then(response => response.data);
  }

  // getManufacturers(params) {
  //   const url = `${API_URL}/manuf`;
  //   return axios.get(url, {params: params}).then(response => response.data)
  // }


}
