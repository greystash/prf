import React from "react";
import logo from "./logo.svg";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import { Navbar, Button, Alignment } from "@blueprintjs/core";

import "./App.scss";
import { page } from "./App.module.scss";
import Home from "./pages/Home/Home.js";
import Bikes from "./pages/Bikes/Bikes.js";

function App() {
  return (
    <Router>
      <Navbar className="bp3-dark">
        <Navbar.Group align={Alignment.RIGHT}>
          <Navbar.Heading> Demo </Navbar.Heading>
          <Navbar.Divider />
          <Link to="/">
            <Button minimal={true} text="Home" />
          </Link>
          <Link to="/bikes">
            <Button minimal={true} text="Bikes" outlined={true} />
          </Link>
        </Navbar.Group>
      </Navbar>
      <div className={page}>
        <Switch>
          <Route path="/" exact={true}>
            <Home />
          </Route>
          <Route path="/bikes">
            <Bikes />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
